<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tubes_wp_db' );

/** MySQL database username */
define( 'DB_USER', 'tubes_wp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'gYn#E7mFM55Ha<g!AZ/^vT(x2zd*=FaE9!Tw!|]C:FD^Z)TWN)amll0FaZ11y:ZP' );
define( 'SECURE_AUTH_KEY',  'v)]!tvNDJ[@sKUrC+,^i2+uZ2sv~2CypifFVD9r`;xl3^x2m+o)w|l8^:Pf#k5C#' );
define( 'LOGGED_IN_KEY',    'a8I8_v=)s80#UA;z2v*Q.A&5UKHtbl<(..h4[wke@s $(&>E;pRY;K4U=0Mr}uqF' );
define( 'NONCE_KEY',        'NPu_JqZg=)Ju_x~B!}X<SdedZo/)!j&Z!QMjT||eZj&QdKaO@!c)jDHr~O]Bg0[s' );
define( 'AUTH_SALT',        '[,BGu+u-MF$OY:,LLK<-NrmJ1qYzR*8Xccg-qYK9en(8;i/^EAz}K08}P[*xkYc8' );
define( 'SECURE_AUTH_SALT', 'z,yCjSy_2o!1aR&#OR/|hvd @iL!NZ7W94P[-SKXIPN1sFo14b&J$m{t96+]PEsz' );
define( 'LOGGED_IN_SALT',   'MV,BO%/qUQEJYhk=iDP3H#T4?jFcm-?FeZU48N]Fm_k73Uuq)]~`-Z[ZY3T-O.4a' );
define( 'NONCE_SALT',       '*#jK+u-G-j}y.s2Q81Y/BGn7tnb[0)y0a}dQ^pOUdR$t#Xq|Vm/)gqN2BNuQKF7*' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

define('WP_ALLOW_REPAIR', true);

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
